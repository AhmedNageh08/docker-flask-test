variable "image_name" {
  description = "Docker image name"
  type        = string
  default     = "ahmednageh08/flask-demo"
}

variable "image_tag" {
  description = "Docker image tag"
  type        = string
}
variable "SSH_PRIVATE_KEY" {
  description = "ssh connection key"
  type        = string
}
