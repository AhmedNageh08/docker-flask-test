resource "aws_instance" "flask" {
  ami = "ami-0c7217cdde317cfec"
  instance_type = "t2.micro"
  key_name      = "aws-key"
  vpc_security_group_ids = ["sg-0adffdf1bd78587f6"]
  count = 2

  tags = {
    Name = "flask-project"
    
  }
  #provisioner "remote-exec" {
  #  inline = [
  #    "sudo docker run -d -p 5000:5000 ahmednageh08/${var.image_name}:${var.image_tag}"
  #  ]
  #  connection {
  #    type        = "ssh"
  #    user        = "ubuntu" // Replace with the appropriate SSH user for your AMI
  #    private_key = var.SSH_PRIVATE_KEY // Reference the GitLab CI/CD variable for the private key
  #    host        = self.public_ip // Use the public IP of the instance
  #  }
  #}

}
